//
//  GameScene.swift
//  SwiftTwo
//
//  Created by Teddy Li on 11/29/15.
//  Copyright (c) 2015 TXL Projects. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var score: Int = 0
    var jumpHeight: Float = 1.2
    var pName: String = "TL"
    var isAlive: Bool = true
    var isCompLvl1: Bool = false
    var isGamePaused: Bool = false
    
    let myLabel = SKLabelNode(fontNamed:"Futura") //constant, as opposed to var
    let sprite = SKSpriteNode(imageNamed:"Spaceship")
    
    
    override func didMoveToView(view: SKView) {
        printLabel()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        for touch in touches {
            let location = touch.locationInNode(self)
            
            sprite.xScale = 0.1
            sprite.yScale = 0.25
            sprite.position = location
            
            let action = SKAction.rotateByAngle(CGFloat(M_PI), duration:1)
            
            sprite.runAction(SKAction.repeatActionForever(action))
            
            score = score + 1
            pName = "new Name"
            isAlive = false
            
            removePlane()
            changeLabel()
            self.addChild(sprite)
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func printLabel(){
        
        myLabel.text = "TXL Projects";
        myLabel.fontSize = 45;
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
        
        self.addChild(myLabel)
    }
    
    func changeLabel(){
        myLabel.text = "\(score) \(pName) \(isAlive)";
        
    }
    
    func removePlane(){
        sprite.removeFromParent()
    }
    
}
